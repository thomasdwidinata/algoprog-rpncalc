# Algorithm and Programming (COMP 6047)

## RPN Calculator / Assignment 2

### Description
The purpose of this assignment is to familiarise students with arrays and function calling using C Programming Language. The program will accept 4 number maximum and will expect to receive operators to calculate the numbers.

### Compiling the Source Code
To compile the source code, we can simply execute the following command:

`gcc *.c`

The `*.c` will compile everything on current folder that has extension `.c`. Since there are only 1 source code, it is easier to use `*.c`

### How to run
Simply open the binary file that has been produced by the `gcc` compiler command. The default name is `a.out`. The first thing to do is to insert 4 integer numbers or less, then after the instructions changed until you can insert operators, you can either insert a number or operators. But if you already insert 4 numbers, you are not allowed to insert any more numbers and you must insert operators as the instruction on screen has said.

## Known bugs
There are no input validations and calculation validation such as division by 0. The program overall is useful if and only if the user input are correct, otherwise it will produce calculation bugs somehow. The source code was built on Windows machine and the command `system("cls")` is still present. Mac and Linux system would probably complaint what `cls` is.
