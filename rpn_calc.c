#include <stdio.h>
#include <string.h>

/* RPN Calculator v1.1
Thomas Dwi Dinata
1901498151 */

 /* -------------------------------------------------------------------
 
							RPN Calculator

							Release Notes:

	v1.1
	Deletes unused variable
	Improved code performance
	Added comments to the source code
	More GUI and much more friendlier user interface

	v1.0
	First release of the program

	v0.1 Beta
	Many stupid bugs here... Like logic errors and calculation problems

    ------------------------------------------------------------------- */

int a; // Array Pointer and also array pointer indicator to other functions (Useful for all functions !)
int b; // Used for saving 'a' value for re-printing Array (Used in operator() and printer() function)
float arr[4]; // Number Arrays
char arrc[10]; // String Arrays, combined with numbers that represents an ASCII
float sum; // Sum of the array (Used in operator() function)
int number; // Saving numbers from arrc[a] and then on numchecker() function, it is used to be assigned to arr[a]
int h; // Printer Helper for array pointer
		
void instPrinter() // This function is used for printing the GUI to tell the user to enter numbers or operator or both of them. Should be printed when the array has been pushed by numbers
{
	if(a>=3) // If the pointer gets to number 3 (Which is the fourth number in the array arr[a]), it will prompt the user to input an operator
	{
		printf("------------------*\nEnter operator : ");
	}
	else if (a<1) // If the pointer still pointing to number 0 (Which is the first array on arr[a]), it will prompt the user to input another number in order to complete the calculation
	{
		printf("------------------*\nEnter a number : ");
	}
	else if (a>0 && a<4) // If the pointer is pointing at number 1 and 2 (Which is the second array and the third array), it will prompt the user to input number or operator
	{
		printf("------------------*\nEnter a number / operator : ");
	}
}
	
void operator(int AC) // Operator is used to calculate the latest array with second latest array 
{
	switch(AC)
	{
	case 1: // Case 1 is Add
		--a;
		sum = arr[a]+arr[a-1];
		--a;
		arr[a] = sum;
		system("cls");
		b=a;
		a=0;
		while (a<=b)
		{
			printf("------------------*\n");
			printf("%g\n",arr[a]);
			++a;
		}
		--a;
		break;
	case 2: // Case 2 is subtract
		--a;
		sum = arr[a]-arr[a-1];
		--a;
		arr[a] = sum;
		system("cls");
		b=a;
		a=0;
		while (a<=b)
		{
			printf("------------------*\n");
			printf("%g\n",arr[a]);
			a++;
		}
		--a;
		break;
	case 3: // Case 3 is multiplication
		--a;
		sum = arr[a]*arr[a-1];
		--a;
		arr[a] = sum;
		system("cls");
		b=a;
		a=0;
		while (a<=b)
		{
			printf("------------------*\n");
			printf("%g\n",arr[a]);
			++a;
		}
		--a;
		break;
	case 4: // Case 4 is division
		--a;
		sum = arr[a]/arr[a-1];
		--a;
		arr[a] = sum;
		system("cls");
		b=a;
		a=0;
		while (a<=b)
		{
				printf("------------------*\n");
				printf("%g\n",arr[a]);
				++a;
		}
		--a;
		break;
	default:
		printf("Invalid Operator! Use '+' or '-' or '*' or '/'\n");
		--a;
		break;
	}
}

void printer() // Printer used to reprint all the arrays including the seperator after user inserted a number
{
	system("cls");
	h=a;
	b=0;
	printf("------------------*\n");
	while(b<=h)
	{
		
		printf("%g\n",arr[b]);
		printf("------------------*\n");
		b++;
	}
}

void numpush() // Pushing the number from 'number' variable to array arr[a]
{
	if(a>=4)
	{
		printf("\n\n*****Error!*****\nStack is full!\nTerminating...");
	}
	else
	{
		arr[a]=number;
		printer();
	}
}

void help(); // Declaring help() function (SEE BELOW)

void checker() // String checker, this function works by comparing the string entered by user into these little database
{
	if(strncmp(arrc, "+", 1) == 0)
	{
        operator(1);
    }
	else if(strncmp(arrc, "-", 1) == 0)
	{
        operator(2);
    }
    else if(strncmp(arrc, "*", 1) == 0)
	{
        operator(3);
    }
	else if(strncmp(arrc, "/", 1) == 0)
	{
        operator(4);
    }
    else if(strncmp(arrc, "help", 1) == 0) // Display Help
    {
    	system("cls");
		help();
	}
    else
	{
        sscanf(arrc,"%d",&number); //Converting all strings to integer
        numpush();
    }
    instPrinter();
}

void main()
{
	printf("\t*** RPN Calculator ***\n\n   By Thomas Dwi Dinata (1901498151)\n---------------------------------------\n\n");
	printf("Enter maximum 4 numbers and Operator\nType 'help' if you need any help\n\nEnter 4 numbers:\n");
	a=0;
	while(a<5)
	{
		fgets(arrc, 10, stdin);
		checker();
		a++;
	}
}

void help()
{
	arr[0]=0;
	arr[1]=0;
	arr[2]=0;
	arr[3]=0;
	a=0;
	printf("\nRPN Calculator v1.1\nBy Thomas Dwi Dinata (1901498151)\n\n\nReverse Polish Notation (RPN) calculator is a calculator that calculates\nlike polish mathematical notation, just reversed. So the first number you\nentered will be calculated last and the last number you entered will be\ncalculated first. RPN Calculator works if you've entered minumum 2 numbers. After you've entered 2 numbers, you can do calculation using + for add, - for subtract, * for multiplication, and / for division.\nREMEMBER! Maximum numbers is 4 !\n\n");
	system("pause");
	system("cls");
	main();
}
